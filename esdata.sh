#!/bin/bash

# Download elasticsearch data parts
for i in {0..44}
do
   link="https://raw.githubusercontent.com/hysds/puppet-grq/master/files/elasticsearch-data.tbz2."
   id=""
   if [ "$i" -lt 10 ]
   then
      id="0"$i
   else
      id=$i
   fi
   wget $link$id -P /etc/puppet/modules/grq/files/
done

# Join the parts and extracts it
cat /etc/puppet/modules/grq/files/elasticsearch-data.tbz2.* > /tmp/elasticsearch-data.tbz2

# Untar the tarball
tar xjvf /tmp/elasticsearch-data.tbz2 -C /var/lib

# Fix permissions
chown -R elasticsearch:elasticsearch /var/lib/elasticsearch

# Restart elasticsearch service
systemctl restart elasticsearch
